jwires/Main.class: jwires/*.java
	javac jwires/*.java

main: jwires/Main.class
	java jwires.Main

clean:
	rm -f jwires/*.class

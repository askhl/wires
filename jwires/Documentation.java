/*
 * Copyright 2007 Ask Hjorth Larsen
 *
 * asklarsen@gmail.com
 *
 * This file is part of Wires.
 *
 * Wires is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wires is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wires.  If not, see <http://www.gnu.org/licenses/>.
 */

package jwires;

import java.io.*;
import java.util.Properties;

/**
 * @author Ask Hjorth Larsen
 */
public class Documentation extends Properties {
    
    public static Documentation doc = new Documentation();
    
    private Documentation() {
        try{
            InputStream in = getClass().getResourceAsStream("help.txt");
            BufferedReader r = new BufferedReader(new InputStreamReader(in));
            
            StringBuffer buf = new StringBuffer();

            String s;
            while((s = r.readLine()) != null){
                buf.append(s+"\n");
            }

            String fullText = buf.toString();
            
            String[] keysAndValues = fullText.split("======");
            for(int i = 1; i < keysAndValues.length;){ // First element will be empty string, so skip
                setProperty(keysAndValues[i++].trim(), keysAndValues[i++].trim());
            }
        } catch(Exception ex){
            ex.printStackTrace();
            System.exit(-1);
        }
    }
        
}

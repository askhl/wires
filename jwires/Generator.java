/*
 * Copyright 2007 Ask Hjorth Larsen
 *
 * asklarsen@gmail.com
 *
 * This file is part of Wires.
 * 
 * Wires is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wires is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wires.  If not, see <http://www.gnu.org/licenses/>.
 */

package jwires;

import java.util.Random;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author ask
 */
public class Generator {
    
    long seed;
    Random random;
    
    public Generator() {
        setRandomSeed();
    }

    public void setRandomSeed(){
        random = new Random();
        setSeed(random.nextLong()); // such that we know which seed it has
    }
    
    public void setSeed(long seed){
        this.seed = seed;
    }
    
    public void generate(TileMap map){
        random.setSeed(seed);
        Tile[] vertices = map.getTiles();
        ArrayList<Edge> edgeCandidates = new ArrayList<Edge>(2*map.width*map.height);
        
        for(Tile tile : vertices){
            edgeCandidates.add(new Edge(tile, 0));
            edgeCandidates.add(new Edge(tile, 3));
        }
        
	ArrayList<Edge> reserveEdges = new ArrayList<Edge>();
        Collections.shuffle(edgeCandidates, random);
        for(Edge edge : edgeCandidates){
            if(isAcceptableConnection(edge.vertex1, edge.vertex2)){
		if(edge.vertex1.getConnectionCount() < 3 &&
		   edge.vertex2.getConnectionCount() < 3){
		    edge.vertex1.connect(edge.direction);
		} else {
		    reserveEdges.add(edge);
		}
            }
        }
	for(Edge edge: reserveEdges){
	    if(isAcceptableConnection(edge.vertex1, edge.vertex2)){
		edge.vertex1.connect(edge.direction);
	    }
	}
        for(Tile tile : vertices){
            tile.validate();
        }
    }
    
    public void shuffle(TileMap map){
        for(Tile tile : map.getTiles()){
            tile.rotate(random.nextInt(4));
        }
    }
    
    public long getSeed(){
        return seed;
    }
    
    public boolean isAcceptableConnection(Tile t1, Tile t2){
        return t1.getGroup() != t2.getGroup() && t1 != TileMap.EDGE_OF_WORLD && t2 != TileMap.EDGE_OF_WORLD;
    }
    
    public class Edge {
        int direction;
        Tile vertex1, vertex2;
        
        public Edge(Tile vertex, int direction){
            this.direction = direction;
            this.vertex1 = vertex;
            this.vertex2 = vertex.neighbours[direction];
        }
        
        /*public boolean wraps(){
            return Math.abs(vertex1.x - vertex2.x) > 1 || Math.abs(vertex1.y - vertex2.y) > 1;
        }*/
        
    }
    
}

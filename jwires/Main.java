/*
 * Copyright 2007 Ask Hjorth Larsen
 *
 * asklarsen@gmail.com
 *
 * This file is part of Wires.
 * 
 * Wires is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wires is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wires.  If not, see <http://www.gnu.org/licenses/>.
 */

package jwires;

import java.text.NumberFormat;
import java.util.Arrays;

/**
 *
 * @author ask
 */
public class Main {
    
    public final static String VERSION = "1.01";
    public final static String NAME = "Wires";
    
    final WireFrame frame;
    final Generator generator = new Generator();
    Board board;
    TileMap map;
    public final static String stdArgs = "6x6";
    
    long gameTimeMillis;
    
    public Main(String[] args) {
        frame = new WireFrame(this);
        GameOptions o = parseOptions(args);
        startGame(o);
        frame.setVisible(true);
    }
    
    public GameOptions parseOptions(String options){
        if("".equals(options)){
            options = stdArgs;
        }
        return parseOptions(options.split("\\s"));
    }
    
    public GameOptions parseOptions(String[] args){
        if(args.length == 0){
            return parseOptions(stdArgs);
        } else {
            String[] widthHeight = args[0].split("x");
            
            int width = Integer.parseInt(widthHeight[0]);
            int height = Integer.parseInt(widthHeight[1]);
            
            boolean shuffle = true;
            Topology topology = Topologies.plane;
            long seed = 0L;
            boolean setSeed = false;
            
            for(int i = 1; i < args.length; i++){
                if("noshuffle".equals(args[i])){
                    shuffle = false;
                } else if(args[i].startsWith("topology")){
                    topology = Topologies.valueOf(args[i].split(":")[1]);
                } else if(args[i].startsWith("seed")){
                    seed = Long.parseLong(args[i].split(":")[1]);
                    setSeed = true;
                }
            }
            return new GameOptions(width, height, shuffle, topology, seed, setSeed);
        }
    }
        
    public void startGame(GameOptions o){
        if(o.useSeed){
            generator.setSeed(o.seed);
        } else {
            generator.setRandomSeed();
        }
        map = new TileMap(o.width, o.height, o.topology);
        generator.generate(map);
        if(o.shuffle){
            generator.shuffle(map);
        }
        board = new Board(map, this);
        frame.setBoard(board);
        gameTimeMillis = System.currentTimeMillis();
    }
    
    public int getGameTime(){
        return (int)((System.currentTimeMillis() - gameTimeMillis)/1000);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Main main = new Main(args);
    }
    
    public static class GameOptions {
        
        public final int width, height;
        public final boolean shuffle;
        public final Topology topology;
        public final long seed;
        public final boolean useSeed;
        
        public GameOptions(int width, int height, boolean shuffle, Topology topology, long seed, boolean useSeed){
            this.width = width;
            this.height = height;
            this.shuffle = shuffle;
            this.topology = topology;
            this.seed = seed;
            this.useSeed = useSeed;
        }
        
    }
    
}

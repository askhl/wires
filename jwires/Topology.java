/*
 * Copyright 2007 Ask Hjorth Larsen
 *
 * asklarsen@gmail.com
 *
 * This file is part of Wires.
 * 
 * Wires is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wires is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wires.  If not, see <http://www.gnu.org/licenses/>.
 */

package jwires;

/**
 *
 * @author ask
 */
public interface Topology {
    
    public final static int TERM = 0, WRAP = 1, FLIP = 2, OTHER = 3;
    
    public void assign(Tile[] north, Tile[] south, Tile[] east, Tile[] west);
    
    public void pushRow(Tile[] tiles, int direction);
    public int getBoundaryType(int direction);
    public boolean isShiftable(int direction);
    
    public String getDescription();
    
}

/*
 * Copyright 2007 Ask Hjorth Larsen
 *
 * asklarsen@gmail.com
 *
 * This file is part of Wires.
 * 
 * Wires is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wires is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wires.  If not, see <http://www.gnu.org/licenses/>.
 */

package jwires;

/**
 *
 * @author ask
 */
public enum Topologies implements Topology {//extends Topology {

    plane(TERM, TERM, "Plane"),
    cylinder(WRAP, TERM, "Cylinder x"),
    cylinder2(TERM, WRAP, "Cylinder y"),
    torus(WRAP, WRAP, "Torus"),
    moebius(FLIP, TERM, "Möbius band x"),
    moebius2(TERM, FLIP, "Möbius band y"),
    klein(FLIP, WRAP, "Klein bottle x"),
    klein2(WRAP, FLIP, "Klein bottle y"),
    realprojectiveplane(FLIP, FLIP, "Real proj. plane"){ 
        // This one cannot be shifted since some tiles have the same neighbour in two directions
        public boolean isShiftable(int direction){
            return false;
        }
    };
    
    private final int horizontal, vertical;
    private final String name;
    private final int[] policies = new int[4];
    private final static String[] policyNames = {"terminates", "wraps", "flips", "other"};
    
    Topologies(int horizontalPolicy, int verticalPolicy, String name){
        policies[0] = policies[2] = this.horizontal = horizontalPolicy;
        policies[1] = policies[3] = this.vertical = verticalPolicy;
        this.name = name;
    }
    
    public void assign(Tile[] north, Tile[] south, Tile[] east, Tile[] west) {
        if(horizontal == TERM){
            for(int i = 0; i < east.length; i++){
                east[i].neighbours[0] = TileMap.EDGE_OF_WORLD;
                west[i].neighbours[2] = TileMap.EDGE_OF_WORLD;
            }
        } else if(horizontal == FLIP){
            glueFlipped(east, west, 0);
        }
        if(vertical == TERM){
            for(int i = 0; i < north.length; i++){
                north[i].neighbours[1] = TileMap.EDGE_OF_WORLD;
                south[i].neighbours[3] = TileMap.EDGE_OF_WORLD;
            }
        } else if(vertical == FLIP){
            glueFlipped(north, south, 1);
        }
    }
    
    private void glueFlipped(Tile[] first, Tile[] second, int direction){
        int len = first.length;
        for(int i = 0; i < len; i++){
            int reverse = (direction+2)%4;
            first[i].neighbours[direction] = second[len-i-1];
            second[len-i-1].neighbours[reverse] = first[i];
        }
    }
    
    public boolean isShiftable(int direction){
        return policies[direction] != TERM;
    }

    public int getBoundaryType(int direction) {
        return policies[direction];
    }

    public void pushRow(Tile[] tiles, int direction) {
        if(policies[direction] == TERM){
            throw new RuntimeException("Cannot push row beyond edge!");
        } else if(policies[direction] == FLIP){
            int left = (direction + 1) % 4;
            int right = (direction + 3) % 4;
            for(Tile tile : tiles){
                // swap around neighbours
                Tile temp = tile.neighbours[left];
                tile.neighbours[left] = tile.neighbours[right];
                tile.neighbours[right] = temp;
                
                int leftRotated = (left + tile.orientation) % 4;
                int rightRotated = (right + tile.orientation) % 4;
                
                // swap around internal tile properties
                boolean tempConnection = tile.connections[leftRotated];
                tile.connections[leftRotated] = tile.connections[rightRotated];
                tile.connections[rightRotated] = tempConnection;
            }
        } // or else it wraps, which means nothing happens
    }
    
    public String toString(){
        return name;
    }
    
    public String getDescription(){
        return "North-south: "+policyNames[vertical]+", East-West: "+policyNames[horizontal];
    }
    
}

/*
 * Copyright 2007 Ask Hjorth Larsen
 *
 * asklarsen@gmail.com
 *
 * This file is part of Wires.
 * 
 * Wires is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wires is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wires.  If not, see <http://www.gnu.org/licenses/>.
 */

package jwires;

import java.util.Arrays;


/**
 *
 * @author ask
 */
public class TileMap {
    
    public final int width, height;
    private final Tile[] tiles;
    
    public final static int FILLED_INDEX = -1;
    public final static int EDGE_OF_WORLD_INDEX = -2;
    
    public final static Tile EDGE_OF_WORLD = new Tile(EDGE_OF_WORLD_INDEX, -1, -1){
        public void setGroup(int group){
            throw new RuntimeException("Cannot set group for edge of world!");
        }
        public void connect(int direction){
            throw new RuntimeException("Cannot connect to edge of world!");
        }
        public String toString(){
            return "Tile: Edge of world!";
        }
    };
    
    public final Topology topology;
    
    // This is a pseudo-tile which cannot be connected to anything. If the world has edges, this tile is
    // found on all edges.
    /*public final static Tile EDGE_OF_WORLD = new Tile(-1,-1,-1){
        public void connect(int direction){
            throw new RuntimeException("Cannot connect with edge of world!");
        }
    };*/
    
    public TileMap(int width, int height){
        this(width, height, Topologies.plane);
    }
    
    /** Creates a new instance of TileMap */
    public TileMap(int width, int height, Topology topology) {
        this.width = width;
        this.height = height;
        this.topology = topology;
        tiles = new Tile[width*height];
        for(int i = 0; i < tiles.length; i++){
            tiles[i] = new Tile(i, i%width, i/width);
        }
        /*for(int i = 0; i < height; i++){
            System.out.println();
            for(int j = 0; j < width; j++){
                System.out.print(tiles[i + width*j]);
            }
        }*/
        
        assignNeighbours();
        
        for(Tile tile : tiles){
            tile.validate();
        }
        
        Tile[] north = new Tile[width], south = new Tile[width], east = new Tile[height], west = new Tile[height];
        for(int i = 0; i < width; i++){
            north[i] = tiles[i];
            south[i] = north[i].neighbours[1];
        }
        for(int i = 0; i < height; i++){
            west[i] = tiles[i*width];
            east[i] = west[i].neighbours[2];
        }
        
        topology.assign(north, south, east, west);
    }
    
    public Tile getTile(int x, int y){
        return tiles[(x % width) + width*(y % height)];
    }
    
    public void assignNeighbours(){
        for(Tile tile : tiles){
            // Add width and height in order to ensure positive values such that
            // the % operator does not screw things up
            tile.neighbours[0] = getTile(width + tile.x + 1, height + tile.y);
            tile.neighbours[1] = getTile(width + tile.x, height + tile.y - 1);
            tile.neighbours[2] = getTile(width + tile.x - 1, height + tile.y);
            tile.neighbours[3] = getTile(width + tile.x, height + tile.y + 1);
        }
    }
    
    public Tile[] getEdge(int direction){
        Tile[] tiles;
        switch(direction % 4){
            case 0:
                tiles = new Tile[height];
                for(int i = 0; i < height; i++){
                    tiles[i] = getTile(width-1, i);
                }
                break;
            case 1:
                tiles = new Tile[width];
                System.arraycopy(this.tiles, 0, tiles, 0, width);
                break;
            case 2:
                tiles = new Tile[height];
                for(int i = 0; i < height; i++){
                    tiles[i] = getTile(0, i);
                }
                break;
            case 3:
                tiles = new Tile[width];
                System.arraycopy(this.tiles, this.tiles.length-width, tiles, 0, width);
                break;
            default:
                tiles = null; // not reached
        }
        return tiles;
    }
    
    public void shift(int direction){
        direction = (direction + 2) % 4;
        Tile[] oldTiles = getTiles();
        int srcIndex=0, dstIndex=0, length=0;
        for(int i = 0; i < tiles.length; i++){
            tiles[i] = oldTiles[i].neighbours[direction];
        }
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                Tile tile = tiles[i + width*j];
                tile.x = i;
                tile.y = j;
            }
        }
        Tile[] edge = getEdge(direction);
        topology.pushRow(edge, direction);
    }
    
    public void clearMarks(){
        for(int i = 0; i < tiles.length; i++){
            tiles[i].marked = false;
            tiles[i].group = i;
        }
    }
    
    public int floodFill(int x, int y){
        Tile tile = getTile(x, y);
        int count = tile.markConnected(-1);
        return count;
    }
    
    public Tile[] getTiles(){
        return tiles.clone();
    }
    
}

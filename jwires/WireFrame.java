/*
 * Copyright 2007 Ask Hjorth Larsen
 *
 * asklarsen@gmail.com
 *
 * This file is part of Wires.
 * 
 * Wires is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wires is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wires.  If not, see <http://www.gnu.org/licenses/>.
 */

package jwires;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.event.*;
import java.awt.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author ask
 */
public class WireFrame extends JFrame {
    
    private Board board;
    private KeyListener listener;
    private final JPanel panel;
    private final Main main;
    
    private final SettingsFrame settingsFrame = new SettingsFrame();
    private final JLabel topologyLabel = new JLabel();    
    
    private final JButton[] shiftButtons = {new JButton("Right"), 
            new JButton("Up"), new JButton("Left"), new JButton("Down")};
            
    public WireFrame(final Main main) {
        super(Main.NAME+" "+Main.VERSION);
        this.main = main;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        add(panel);
        
        JMenuBar bar = new JMenuBar();
        
        JMenu gameMenu = new JMenu("Game");
        JMenu helpMenu = new JMenu("Help");
        bar.add(gameMenu);
        bar.add(helpMenu);
        setJMenuBar(bar);
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        
        for(int i = 0; i < 4; i++){
            final int direction = i;
            shiftButtons[i].setToolTipText("Displace the board if the topology allows it.");
            shiftButtons[i].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    board.shift(direction);
                    board.repaint();
                    WireFrame.this.requestFocusInWindow();
                }
            });
        }
        
        toolBar.add(shiftButtons[1]);
        toolBar.add(shiftButtons[3]);
        toolBar.add(shiftButtons[2]);
        toolBar.add(shiftButtons[0]);

        toolBar.addSeparator();
        toolBar.add(topologyLabel);
        
        add(toolBar, BorderLayout.NORTH);
        
        String[] helpItems = new String[]{"About", "Instructions", "Topologies", "License"};
        boolean[] useEditorPane = new boolean[]{false, true, true, false};
        
        for(int i = 0; i < helpItems.length; i++){
            final String documentationKey = helpItems[i];
            final String documentation = Documentation.doc.getProperty(documentationKey);
            final Object msg;
            if(useEditorPane[i]){
                final JTextPane textPane = new JTextPane();
                
                textPane.setEditorKit(new javax.swing.text.html.HTMLEditorKit());
                textPane.setText(documentation);
                
                textPane.setOpaque(false);
                textPane.setEditable(false);
                final JScrollPane scr = new JScrollPane(textPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                msg = scr;
            } else {
                msg = documentation;
            }
            
            final boolean forceMaxDialogSize = useEditorPane[i];
            makeItem(helpMenu, helpItems[i], new ActionListener(){
                JOptionPane pane = new JOptionPane(msg, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION){
                    public int getMaxCharactersPerLineCount(){
                        return 60;
                    }
                };
                
                public void actionPerformed(ActionEvent e){
                    
                    JDialog dia = pane.createDialog(WireFrame.this, documentationKey);
                    
                    if(forceMaxDialogSize){
                        dia.setSize(new Dimension(560, 540));
                    }
                    dia.setVisible(true);
                }
            });
        }
        
        makeItem(gameMenu, "New", new ActionListener(){
            public void actionPerformed(ActionEvent e){
                settingsFrame.setVisible(true);
            }
        });
        
        gameMenu.addSeparator();
        
        makeItem(gameMenu, "Exit",new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.exit(0);
            }
        });
    }
    
    public void makeItem(JMenu menu, String name, ActionListener action){
        JMenuItem item = new JMenuItem(name);
        item.addActionListener(action);
        menu.add(item);
    }
    
    private static final Color[] borderColors = {
        new Color(160, 160, 160), 
        Color.white, 
        new Color(0, 222, 128), 
        Color.black}; //The last one is not used
    
    public void setBoard(Board board){
        if(this.board != null){
            this.removeKeyListener(listener);
        }
        this.listener = board.getKeyListener();
        addKeyListener(listener);
        settingsFrame.widthSpinner.setValue(new Integer(main.map.width));
        settingsFrame.heightSpinner.setValue(new Integer(main.map.height));
        settingsFrame.seedSpinner.setValue(new Long(main.generator.getSeed()));
        
        topologyLabel.setText("Topology: "+main.map.topology.toString());
        topologyLabel.setToolTipText(main.map.topology.getDescription());
        
        this.board = board;
        panel.removeAll();
        panel.add(board, BorderLayout.CENTER);
        
        Topology t = board.getTopology();
        
        Color nsColor = borderColors[t.getBoundaryType(1)]; //north == 1
        Color ewColor = borderColors[t.getBoundaryType(0)]; //east == 0
        
        int size = 4;
        
        Border nsBorder = BorderFactory.createMatteBorder(size, 0, size, 0, nsColor);
        Border ewBorder = BorderFactory.createMatteBorder(0, size, 0, size, ewColor);
        
        panel.setBorder(BorderFactory.createCompoundBorder(nsBorder, ewBorder));
        
        for(int i = 0; i < shiftButtons.length; i++){
            shiftButtons[i].setEnabled(main.map.topology.isShiftable(i));
        }
        
        pack();
    }
    
    private class SettingsFrame extends JFrame implements ChangeListener, ActionListener {
        
        final JTextField optionField = new JTextField();
        final JSpinner widthSpinner = new JSpinner(), heightSpinner = new JSpinner();
        final JSpinner seedSpinner = new JSpinner();
        final JCheckBox customSeedBox = new JCheckBox();
        final JCheckBox shuffleBox = new JCheckBox();
        final JComboBox topologyBox = new JComboBox(Topologies.values());
        
        public SettingsFrame(){
            super("New game");
            setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            add(panel);
            
            JPanel guiConfigPanel = new JPanel();
            guiConfigPanel.setBorder(BorderFactory.createTitledBorder("Game options"));
            guiConfigPanel.setLayout(new GridLayout(6, 2));
            panel.add(guiConfigPanel);
            
            topologyBox.setPreferredSize(new Dimension(0, 0));
            
            shuffleBox.setSelected(true);
            
            JPanel seedPanel = new JPanel();
            seedPanel.setLayout(new BoxLayout(seedPanel, BoxLayout.X_AXIS));
            seedPanel.add(customSeedBox);
            
            JPanel seedSubPanel = new JPanel(new GridLayout(1,1));
            seedSubPanel.add(seedSpinner);
            
            seedPanel.add(seedSubPanel);
            
            customSeedBox.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    boolean useCustomSeed = customSeedBox.isSelected();
                    seedSpinner.setEnabled(useCustomSeed);
                }
            });
                        
            seedSpinner.setEnabled(customSeedBox.isSelected());
            topologyBox.setPreferredSize(new Dimension(100, 0));
            topologyBox.setMaximumRowCount(12);
            
            JPanel p = guiConfigPanel;
            JLabel wLabel = new JLabel("Width"),
                    hLabel = new JLabel("Height"),
                    shuffleLabel = new JLabel("Shuffle"),
                    topologyLabel = new JLabel("Topology"),
                    customSeedLabel = new JLabel("Custom seed"),
                    seedLabel = new JLabel("Seed");
            
            wLabel.setToolTipText("Determines the width of the board");
            hLabel.setToolTipText("Determines the height of the board");
            shuffleLabel.setToolTipText("Whether to shuffle the board after generation.");
            
            String topologyHelp = "<html>The board topology.<p>" +
                    "The north-south and east-west border pairs can be<p>" +
                    "solid, wrapping or wrapping plus mirroring</html>";
                        
            topologyLabel.setToolTipText(topologyHelp);
            
            customSeedLabel.setToolTipText("<html>Whether a custom seed should be used to generate<p>" +
                    "the map rather than a random one.</html>");
            seedLabel.setToolTipText("<html>The seed used to generate the puzzle.<p>" +
                    "This value uniquely determines the generated puzzle for any<p>" +
                    "fixed combination of other options.</html>");
            
            p.add(wLabel);
            p.add(widthSpinner);
            p.add(hLabel);
            p.add(heightSpinner);
            p.add(shuffleLabel);
            p.add(shuffleBox);
            p.add(topologyLabel);
            p.add(topologyBox);
            p.add(customSeedLabel);
            p.add(customSeedBox);
            p.add(seedLabel);
            p.add(seedSpinner);
                        
            widthSpinner.addChangeListener(this);
            heightSpinner.addChangeListener(this);
            shuffleBox.addActionListener(this);
            seedSpinner.addChangeListener(this);
            topologyBox.addActionListener(this);
            customSeedBox.addActionListener(this);
            
            optionField.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            JPanel borderSubPanel = new JPanel(new GridLayout(1,1));
            borderSubPanel.setBorder(BorderFactory.createTitledBorder("Command line options"));
            borderSubPanel.add(optionField);
            
            optionField.setText(main.stdArgs);
            
            panel.add(borderSubPanel);
            
            JPanel buttonContainer = new JPanel();
            
            JButton createButton = new JButton("Create game");
            JButton cancelButton = new JButton("Cancel");

            panel.add(buttonContainer);
            
            ActionListener createListener = new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    String optionText = optionField.getText();
                    String[] options = optionText.split("\\s");
                    String errMsg = null;
                    Main.GameOptions o;
                    try{
                        o = main.parseOptions(options);
                    } catch(NumberFormatException ex){
                        JOptionPane.showMessageDialog(SettingsFrame.this, 
                                "Cannot create game.\n\nBad option selection or command syntax.", 
                                "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    }                    
                    try{
                        if(o.width < 1 || o.height < 1){
                            errMsg = "Maybe you should try using POSITIVE width and height.";
                        } else if(o.width * o.height < 2){
                            errMsg = "Congratulations, you now have a connected graph with one vertex.";
                        } else {
                            main.startGame(o);
                            if(e.getSource() != optionField){
                                setVisible(false);
                            }
                        }
                    } catch(Exception ex){
                        ex.printStackTrace();
                        errMsg = "Unexpected error, stack trace written to stdout!";
                    }
                    if(errMsg != null){
                        JOptionPane.showMessageDialog(SettingsFrame.this, errMsg, "Error", 
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            };
            
            createButton.addActionListener(createListener);
            optionField.addActionListener(createListener);
            
            cancelButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    setVisible(false);
                }
            });

            buttonContainer.add(createButton);
            buttonContainer.add(cancelButton);
            
            pack();
        }

        public void actionPerformed(ActionEvent e) {
            updateTextField();
        }

        public void stateChanged(ChangeEvent e) {
            updateTextField();
        }

        public void updateTextField(){
            int width = ((Integer)widthSpinner.getValue()).intValue();
            int height = ((Integer)heightSpinner.getValue()).intValue();
            boolean shuffle = shuffleBox.isSelected();
            long seed = ((Number)seedSpinner.getValue()).longValue();
            boolean useCustomSeed = customSeedBox.isSelected();
            String topology = ((Topologies)topologyBox.getSelectedItem()).name();

            
            String command = ""+width+"x"+height+
                    (shuffle ? "" : " noshuffle")+
                    (useCustomSeed ? " seed:"+seed : "") + 
                    (topology.equals(Topologies.plane.toString()) ? "" : " topology:"+topology);

            optionField.setText(command);                
        }
    
    }
    
}

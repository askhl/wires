/*
 * Copyright 2007 Ask Hjorth Larsen
 *
 * asklarsen@gmail.com
 *
 * This file is part of Wires.
 * 
 * Wires is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wires is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wires.  If not, see <http://www.gnu.org/licenses/>.
 */

package jwires;

/**
 *
 * @author ask
 */
public class Tile {
    
    final Tile[] neighbours = new Tile[4];
    final boolean[] connections = new boolean[4];
    int orientation;
    int group;
    int connectionCount;
    int x, y;
    final int id;
    
    boolean locked;
    boolean marked;
    
    public Tile(int index, int x, int y) {
        this.id = index;
        this.group = index;
        this.x = x;
        this.y = y;
    }
    
    public void rotate(int amount){
        orientation = (4 + orientation + amount) % 4;
    }
    
    public boolean isConnected(int direction){
        return connections[(direction + orientation) % 4];
    }
    
    public boolean isMutuallyConnected(int direction){
        return this.isConnected(direction) && neighbours[direction % 4].isConnected(direction+2);
    }
    
    public Tile getNeighbour(int direction){
        return neighbours[direction % 4];
    }
    
    public int getGroup(){
        return group;
    }
    
    public void setGroup(int group){
        if(group > this.group){
            throw new RuntimeException("Cannot assign larger group index!");
        }
        this.group = group;
        for(int i = 0; i < 4; i++){
            if(connections[i] && neighbours[i].group > this.group){
                if(connections[i] && !neighbours[i].connections[(i+2)%4]){
                    System.out.println("BAD!!!!");
                    System.out.println("Group:"+group);
                }
                neighbours[i].setGroup(group);
            }
        }
    }
    
    public int getConnectionCount(){
        return connectionCount;
    }
    
    public void connect(int direction){
        if(isConnected(direction)){
            throw new RuntimeException("Already connected!");
        }
        connections[direction] = true;
        Tile neighbour = neighbours[direction];
        neighbour.connections[(direction+2)%4] = true;
        if(neighbour.group > this.group){
            neighbour.setGroup(this.group);
        } else if(neighbour.group < this.group){
            this.setGroup(neighbour.group);
        }        
        neighbour.connectionCount++;
        connectionCount++;
    }
    
    public String toString(){
        return "Tile["+x+","+y+"]:"+group + " >> "+java.util.Arrays.toString(connections);
    }
    
    public void validate(){
        for(int i = 0; i < 4; i++){
            if(neighbours[i].neighbours[(i+2)%4] != this && neighbours[i] != TileMap.EDGE_OF_WORLD){
                System.out.println("Bad: direction "+i+", opposite "+((i+2)%4));
                System.out.println("  This tile: "+this);
                System.out.println("  Neighbour: "+neighbours[i]);
                System.out.println("  Neighbour's neighbour (should be this): "+neighbours[i].neighbours[(i+2)%4]);
                throw new RuntimeException("Bad neighbours");
            }
        }
    }
        
    /*public int markConnected(int group){
        return markConnected(group, 0);
    }*/
    
    public int markConnected(int group/*, int count*/){
        int count = 0;
        marked = true;
        if(this.group != group){ //not already marked
            count++;
            this.group = group;
            for(int i = 0; i < 4; i++){
                if(isMutuallyConnected(i)){
                   count += neighbours[i].markConnected(group);
                }
            }
        }
        return count;
    }
        
    public boolean isMarked(){
        return marked;
    }
    
    public boolean isLocked(){
        return locked;
    }
    
    public void setLocked(boolean locked){
        this.locked = locked;
    }
    
    public int getX(){
        return x;
    }
    
    public int getY(){
        return y;
    }
    
    public boolean toggleLock(){
        locked = !locked;
        return locked;
    }
    
}

/*
 * Copyright 2007 Ask Hjorth Larsen
 *
 * asklarsen@gmail.com
 *
 * This file is part of Wires.
 * 
 * Wires is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wires is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wires.  If not, see <http://www.gnu.org/licenses/>.
 */

package jwires;

import java.awt.geom.AffineTransform;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author ask
 */
public class Board extends JPanel {
    
    public final static Color
            lockedColor = new Color(128, 128, 128, 96),
            monoValentColor = new Color(0, 150, 130),
            filledColor = new Color(0, 100, 80),
            wireColor = Color.BLACK,
            floodTileColor = new Color(255, 0, 0, 192),
            cursorColor = new Color(255, 0, 0, 72);

    private final TileMap map;
    private final Main main;
    private int tileSize = 60;
    private int connectedCount;

    private Tile floodTile;
    private boolean paintFloodFill;
    
    private boolean useKeyCursor;
    private Tile cursorTile;
    
    //Drawing and tile-fetching parameters
    private double scale, xScale, yScale, xOffset, yOffset;
    private Listener listener;
    
    
    public Board(TileMap map, Main main) {
        this.map = map;
        this.main = main;
        floodTile = map.getTile(0,0); //any tile will do. We just want to check whether they are *all* connected or not
        cursorTile = floodTile;
        setBackground(Color.white);
        listener = new Listener();
        addMouseListener(listener);
        addMouseWheelListener(listener);
        setPreferredSize(new Dimension(tileSize*map.width, tileSize*map.height));
    }

    public KeyListener getKeyListener(){
        return listener;
    }
    
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        AffineTransform backupTransform = g2d.getTransform();
        int width = getWidth();
        int height = getHeight();
        xScale = (double)width / (tileSize * map.width);
        yScale = (double)height / (tileSize * map.height);
        if(xScale < yScale){
            scale = xScale;
            xOffset = 0;
            yOffset = (height - xScale/yScale*height)/2;
        } else {
            scale = yScale;
            yOffset = 0;
            xOffset = (width - yScale/xScale*width)/2;
        }
        
        g2d.translate(xOffset, yOffset);
        g2d.scale(scale,scale);
        
        
        for(int i = 0; i < map.width; i++){
            for(int j = 0; j < map.height; j++){
                drawTile(map.getTile(i, j), g, i*tileSize, j*tileSize);
            }
        }
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if(paintFloodFill){
            drawFloodBlob(g, floodTile.getX()*tileSize, floodTile.getY()*tileSize);
        }
        if(useKeyCursor){
            int x = cursorTile.getX() * tileSize;
            int y = cursorTile.getY() * tileSize;
            g.setColor(cursorColor);
            int offset = tileSize/4;
            g.fillOval(x+offset, y+offset, tileSize-2*offset, tileSize-2*offset);
        }
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g2d.setTransform(backupTransform);
    }
    
    public void drawFloodBlob(Graphics g, int x, int y){
        g.setColor(floodTileColor);
        int size = tileSize / 3;
        g.fillOval(x+tileSize/2-size/2, y+tileSize/2-size/2, size, size);
    }
    
    public Tile getTile(int xPix, int yPix){
        int x = (int)((xPix-xOffset)/scale)/tileSize;
        int y = (int)((yPix-yOffset)/scale)/tileSize;
        if(x >= map.width || y >= map.height){
            return null;
        }
        return map.getTile(x, y);
    }
    
    public void shift(int direction){
        map.shift(direction);
    }
    
    public void drawTile(Tile tile, Graphics g, int x, int y){
        if(tile.isLocked()){
            g.setColor(lockedColor);
            g.fillRect(x, y, tileSize, tileSize);
        }
        
        int chunk = tileSize/5;
        if(tile.getConnectionCount() == 1){
            g.setColor(monoValentColor);
            g.fillRect(x+chunk, y+chunk, 3*chunk, 3*chunk);
        }
        
        g.setColor((paintFloodFill && tile.isMarked()) ? filledColor : wireColor);
        if(tile.isConnected(0)){
            g.fillRect(x+2*chunk, y+2*chunk, 3*chunk, 1*chunk);
        }
        if(tile.isConnected(1)){
            g.fillRect(x+2*chunk, y+0*chunk, 1*chunk, 3*chunk);
        }
        if(tile.isConnected(2)){
            g.fillRect(x+0*chunk, y+2*chunk, 3*chunk, 1*chunk);
        }
        if(tile.isConnected(3)){
            g.fillRect(x+2*chunk, y+2*chunk, 1*chunk, 3*chunk);
        }
    }
    
    public void floodFill(Tile tile){
        this.floodTile = tile;
        floodFill();
    }
        
    public void floodFill(){
        map.clearMarks();
        connectedCount = map.floodFill(floodTile.getX(), floodTile.getY());
    }

    public void checkWin(){
        if(connectedCount == map.width * map.height){
            // All tiles connected. We have a winner.
            for(Tile tile : map.getTiles()){
                tile.setLocked(true);
            }
            Board.this.repaint();
            String msg = "Congratulations!\n\n" + 
                    "Time taken: "+main.getGameTime()+" seconds\n"+
                    "Board size: "+ main.map.width +" by "+main.map.height+
                    "\nTopology: "+map.topology;
            
            JOptionPane.showMessageDialog(null, msg, "Game over", 
                    JOptionPane.WARNING_MESSAGE);
        }        
    }
    
    public void rotate(Tile tile, int amount){
        if(tile == null){
            return;
        }
        if(!tile.isLocked()){
            tile.rotate(amount);
            floodFill();
            checkWin();
        }
        repaint();
    }
    
    public void toggleFloodFilling(Tile tile){
        if(paintFloodFill){
            map.clearMarks();
            paintFloodFill = false;
            repaint();
        } else {
            paintFloodFill = true;
            floodFill(tile);
            repaint();
        }        
    }
    
    public Topology getTopology(){
        return map.topology;
    }
    
    private class Listener implements MouseListener, MouseWheelListener, KeyListener {
        public void mouseClicked(MouseEvent e) {
        }

        public void mousePressed(MouseEvent e) {
            useKeyCursor = false;
            int button = e.getButton();
            Tile tile = getTile(e.getX(), e.getY());
            if(tile == null){
                return;
            }
            if(button == e.BUTTON1){
                tile.toggleLock();
                repaint();
            } else if(button == e.BUTTON3){
                toggleFloodFilling(tile);
            }
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }

        public void mouseWheelMoved(MouseWheelEvent e) {
            useKeyCursor = false;
            Tile tile = getTile(e.getX(), e.getY());
            rotate(tile, e.getWheelRotation());
        }

        public void keyTyped(KeyEvent e) {
        }

        public void keyPressed(KeyEvent e) {
            useKeyCursor = true;
            int code = e.getKeyCode();
            int direction = -1;
            switch(code){
                case KeyEvent.VK_LEFT:
                case KeyEvent.VK_D:
                case KeyEvent.VK_H:
                    direction = 2;
                    break;
                case KeyEvent.VK_RIGHT:
                case KeyEvent.VK_G:
                case KeyEvent.VK_L:
                    direction = 0;
                    break;
                case KeyEvent.VK_UP:
                case KeyEvent.VK_R:
                case KeyEvent.VK_K:
                    direction = 1;
                    break;
                case KeyEvent.VK_DOWN:
                case KeyEvent.VK_F:
                case KeyEvent.VK_J:
                    direction = 3;
                    break;
                case KeyEvent.VK_SPACE:
                    cursorTile.toggleLock();
                    repaint();
                    break;
                case KeyEvent.VK_ENTER:
                    toggleFloodFilling(cursorTile);
                    break;
                case KeyEvent.VK_E:
                case KeyEvent.VK_PAGE_UP:
                case KeyEvent.VK_LESS:
                    int rotation = -1;
                    if(code == KeyEvent.VK_LESS && e.isShiftDown()){
                        rotation = 1; // Difficult to do this in the next block where it should be
                    }                    
                    rotate(cursorTile, rotation);
                    break;
                case KeyEvent.VK_T:
                case KeyEvent.VK_PAGE_DOWN:
                    rotate(cursorTile, +1);
                    break;
            }
            if(direction != -1){ //some attempt to move has taken place
                if(e.isControlDown()){
                    if(map.topology.isShiftable(direction)){
                        shift(direction);
                        repaint();
                    }
                } else {
                    Tile dst = cursorTile.getNeighbour(direction);
                    cursorTile = dst == TileMap.EDGE_OF_WORLD ? cursorTile : dst;
                    repaint();
                }
            }
        }

        public void keyReleased(KeyEvent e) {
        }
        
    }
        
}

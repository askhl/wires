======
License
======
<html><h2>License</h2></html>
Wires is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

Wires is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Wires.  If not, see <http://www.gnu.org/licenses/>

Copyright 2007, Ask Hjorth Larsen

======
About
======
<html><h2>Wires</h2></html>
Rotate tiles to form a connected graph.

Written by Ask Hjorth Larsen.

<html><tt>asklarsen@gmail.com</tt></html>

======
Instructions
======
<style type="text/css">
  body { font-family: DialogInput; }
  h2 {font-family: DialogInput; }
  em { font-style: plain; font-weight: bold }
</style>

<h2>Instructions</h2>
The objective is to <em>rotate</em> the tiles on the board such that they are all connected.
<p>
You can <em>lock</em> tiles, signifying (to yourself, mainly) that they are in the correct state. It is possible to perform a <em>flood fill</em> operation on a tile, which means that all tiles connected to this one are highlighted. The tiles that have only one outgoing connection have special graphics, but that is just for show.
<h2>Controls</h2>
<b>Mouse</b>
<hr width="300" align="left">
If using the mouse it is not also necessary to use the keyboard.
<dl><dt><dd>
    Use the scroll-wheel to rotate tiles<br>
    Left-click to toggle tile lock<br>
    Right-click to flood fill connected tiles<br>
</dl>
<p>
<b>Keyboard</b>
<hr width="300" align="left">
There are several alternative ways to perform most actions.<br>
<dl>
    <dt><b>Cursor movement:</b>
    <dd>left, right, up down (classical)<br>
        h, j, k, l (vi-keys)<br>
        d, g, r, f (wasd improved)<br>

    <dt><b>Tile rotation:</b>
    <dd>PgUp, PgDown<br>
        &lt, &gt<br>
        e, t (for dgrf-controls)<br>
    <dt><b>Others:</b>
    <dd>Space toggles tile lock<br>
        Enter flood fills connected tiles

</dl>
<p>
<b>Miscellaneous</b>
<hr width="300" align="left">
Press the buttons above the board to translate the board coordinate system in a desired direction, if the current topology supports translation. This can also be achieved by holding down Ctrl while using cursor movement buttons.

======
Topologies
======
<style type="text/css">
  body { font-family: DialogInput; }
  h2 {font-family: DialogInput; }
  em { font-style: plain; font-weight: bold }
</style>

<h2>Topologies</h2>
The <em>topology</em> determines what happens at the edges of the board. With the default (planar) topology, the edges terminate the board.
<p>
Other topologies <em>wrap</em>, i.e. have the property that tiles on the some edge of the board can be connected to tiles on the opposite edge. The two edges can be thought of as having been glued together.
<p>
Finally some topologies wrap and <em>flip</em>, meaning that two opposing edges have been glued together, but one of them has its direction reversed. Thus, going up along one edge corresponds to going down on the opposite.
<p>
Note that while using the keyboard controls, the cursor will behave according to the selected topology at the edge of the map.
<p>
<b>Available topologies</b>
<hr width="300" align="left">
Presently only opposing edges can be glued together. Two opposing edges are henceforth referred to as a pair.
<ul>
    <li><em>Plane</em>: All edges terminate
    <li><em>Cylinder</em>: One pair of edges terminates, the other wraps
    <li><em>Torus</em>: All edges wrap
    <li><em>Möbius band</em>: One pair of edges terminates, the other flips
    <li><em>Klein bottle</em>: One pair of edges wraps, the other flips
    <li><em>Real projective plane</em>: All edges flip
</ul>
